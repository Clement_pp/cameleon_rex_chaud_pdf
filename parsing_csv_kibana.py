import csv
import json
import pdb


def valeur_dict_to_str(valeurs):

    """Permet de filtrer les formats[{'libelle': 'DEF ORG'}] pour les sortir en
    str /n"""

    valeur_out = ""

    if type(valeurs) is list:
        for item in valeurs:
            if type(item) is dict:
                for key in item:
                    if key != "code":
                        valeur_out += str(item[key])+'\n'
            else:
                valeur_out += item + '\n'
    else:
        if len(str(valeurs)) > 30000:
            valeurs = str(valeurs)[0:30000]
        valeur_out = str(valeurs).replace("&nbsp", ' ')

    return valeur_out.strip()


def faceGenerique_to_json(jsonfacegenerique):

    """Parse un json des faces génériques contenus dans le csv pour l'exporter
    sous forme de dictionnaire

    json -> dict

    Le dictionnaire de sortie : clé modèle de la face : dictionnaire de la face

    le dictionnaire de la face : nom_champ_valeur
    """

    faces_generique = json.loads(jsonfacegenerique)
    dico_faces = {}
    champ_a_valeur = [
        "champProcessusElementaires",
        "champText",
        "champListeValeurs",
        "champDate",
        "champNombre"
    ]
    # Boucle sur les faces contenues

    for face in faces_generique:
        dico_face = {}

        # Boucle sur les champs de la face
        for key in face.keys():
            if key != "champsFaceGenerique":
                dico_face[key] = face[key]
            else:

                for champ in face[key]:
                    libelle = champ["libelleChamp"]
                    valeur = None

                    for keyChamp in champ.keys():
                        if (keyChamp in champ_a_valeur and
                                champ[keyChamp] is not None):

                            valeur = champ[keyChamp]

                    if valeur is not None:
                        dico_face[libelle] = valeur

        dico_faces[dico_face['modele']] = dico_face

    return dico_faces


def from_kibana_csv_to_pydict(chemin_fichier):

    """Fonction prenant en entrée un chemin vers un fichier csv extrait de kibana
    et qui retourne un dictionnaire contenant tous les constats.

    chemin vers csv -> dict

    Structure du dictionnaire de sortie :

    référence_constat:dictionnaire constat

    Structure dictionnaire constat : nom_champ:valeur

    Attention pour les faces du constat modele_face:dictionnaire_face"""

    csvfile = open(chemin_fichier, 'r', encoding="utf8")
    donnees_cameleon = [line for line in csv.reader(csvfile,
                                                    delimiter=",",
                                                    quotechar='"')]

    header_csv = donnees_cameleon.pop(0)

    _ = 0
    dico_champ = {}
    for item in header_csv:
        if item in dico_champ.keys():
            print("J'ai un probléme de clé")
            print(item)
        else:
            dico_champ[item] = _
            _ += 1

    dico_des_constats = {}
    for line in donnees_cameleon:
        dico_constat = {}
        for key in dico_champ.keys():
            dico_constat[key] = line[dico_champ[key]]

        if 'facesGenerique' in dico_constat.keys():

            faces_generiques = faceGenerique_to_json(
                                    dico_constat["facesGenerique"])

            for modele in faces_generiques.keys():
                dico_constat[modele] = faces_generiques[modele]
        else:
            print("IL y a un constat en erreur")

        dico_des_constats[dico_constat['reference']] = dico_constat

    return dico_des_constats
