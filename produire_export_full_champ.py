import parsing_csv_kibana as csvkb
import pdb
import json

TRS_CHAMP = {
    "_id": 0,
    "_index": 1,
    "_score": 2,
    "_type": 3,
    "actionProposition": 4,
    "applicationSource": 5,
    "applicationSourceLien": 6,
    "codeDivision": 7,
    "codeProjet": 8,
    "codeUnite": 9,
    "commentaires": 10,
    "creeLe": 11,
    "creeParNni": 12,
    "creeParNom": 13,
    "creeParPrenom": 14,
    "creeParSection": 15,
    "creeParService": 16,
    "creeParSite": 17,
    "dateConstat": 18,
    "description": 19,
    "dt": 20,
    "epinglesPar": 21,
    "equipement": 22,
    "equipement.classeSurete": 23,
    "equipement.libelle": 24,
    "equipement.tag": 25,
    "etat": 26,
    "etatStandardReacteur": 27,
    "etatTranche": 28,
    "vide_2": 29,
    "vide_3": 30,
    "vide_4": 31,
    "vide_5": 32,
    "id": 33,
    "indice": 34,
    "local": 35,
    "local.classeSurete": 36,
    "local.libelle": 37,
    "local.tag": 38,
    "localisation.geoPoint.geohash": 39,
    "localisation.geoPoint.lat": 40,
    "localisation.geoPoint.lon": 41,
    "localisation.label": 42,
    "nature": 43,
    "occasionDecouverte": 44,
    "pa": 45,
    "reference": 46,
    "section": 47,
    "service": 48,
    "site": 49,
    "systemeElementaire": 50,
    "titre": 51,
    "totr": 52,
    "tranche": 53,
    "url constat": 54,
    "Déclaration id": 55,
    "Déclaration modele": 56,
    "Déclaration reference": 57,
    "Déclaration etat": 58,
    "Déclaration site": 59,
    "Déclaration siteResponsable": 60,
    "Déclaration serviceResponsable": 61,
    "Déclaration sectionResponsable": 62,
    "Déclaration caracterisations": 63,
    "Déclaration Description de l'événement": 64,
    "Déclaration Facteur supplémentaire éventuel": 65,
    "Déclaration Conséquences potentielles": 66,
    "Déclaration Indice de la déclaration": 67,
    "Déclaration Origine présumé de l'évenement": 68,
    "Déclaration Conséquences réélles": 69,
    "Déclaration Classement de base proposé": 70,
    "Déclaration Classement INES proposé": 71,
    "Déclaration creeLe": 72,
    "Déclaration creeParNom": 73,
    "Déclaration creeParPrenom": 74,
    "Déclaration modeleCode": 75,
    "Déclaration etats": 76,
    "Déclaration creeParNni": 77,
    "Déclaration codeUnite": 78,
    "Déclaration codeDivision": 79,
    "Déclaration Classement INES proposé (RP)": 80,
    "Déclaration Classement de base proposé (RP)": 81,
    "Déclaration Initiateur attendu": 82,
    "Déclaration Fonction de sûreté concernée": 83,
    "Déclaration Facteurs aténuateurs": 84,
    "REX Chaud id": 85,
    "REX Chaud modele": 86,
    "REX Chaud reference": 87,
    "REX Chaud etat": 88,
    "REX Chaud site": 89,
    "REX Chaud siteResponsable": 90,
    "REX Chaud serviceResponsable": 91,
    "REX Chaud sectionResponsable": 92,
    "REX Chaud caracterisations": 93,
    "REX Chaud Libellé de l'évenement": 94,
    "REX Chaud Code détection": 95,
    "REX Chaud Code équipement": 96,
    "REX Chaud Décisions": 97,
    "REX Chaud Pesage": 98,
    "REX Chaud Synthèse de l'évenement": 99,
    "REX Chaud Code evénement": 100,
    "REX Chaud Causes humaines": 101,
    "REX Chaud Cause prépondérante": 102,
    "REX Chaud creeLe": 103,
    "REX Chaud creeParNom": 104,
    "REX Chaud creeParPrenom": 105,
    "REX Chaud modeleCode": 106,
    "REX Chaud etats": 107,
    "REX Chaud creeParNni": 108,
    "REX Chaud codeUnite": 109,
    "REX Chaud codeDivision": 110,
    "REX Chaud Compléments d'informations": 111,
    "REX Chaud Cadre de traitement": 112,
    "REX Chaud Causes matérielles": 113,
    "REX Chaud Code acteur": 114,
    "REX Chaud Indicateur 2.0": 115,
    "REX Chaud Entité pilote": 116,
    "REX Chaud Causes organisationnelles": 117,
    "REX Chaud Échéance de traitement": 118,
    "REX Chaud ID DT208": 119,
    "REX Chaud Appuis": 120,
    "REX Chaud Rattachement": 121,
    "REX Chaud Commentaires GSPN": 122,
    "REX Chaud Causes profondes": 123,
    "REX Froid": 124,
    "REX Froid id": 125,
    "REX Froid modele": 126,
    "REX Froid reference": 127,
    "REX Froid etat": 128,
    "REX Froid site": 129,
    "REX Froid siteResponsable": 130,
    "REX Froid serviceResponsable": 131,
    "REX Froid sectionResponsable": 132,
    "REX Froid caracterisations": 133,
    "REX Froid creeLe": 134,
    "REX Froid creeParNom": 135,
    "REX Froid creeParPrenom": 136,
    "REX Froid modeleCode": 137,
    "REX Froid etats": 138,
    "REX Froid creeParNni": 139,
    "REX Froid codeUnite": 140,
    "REX Froid codeDivision": 141,
    "REX Froid Commentaire": 142,
}

MODELE_EXPORTE = [
    "REX Froid",
    "REX Chaud",
    "Déclaration"
    ]


def export_full(dico_des_constats):

    # Initialisation
    llignes = []
    HEADER = ["-"] * len(TRS_CHAMP)

    for key in TRS_CHAMP:
        HEADER[TRS_CHAMP[key]] = key

    llignes.append(HEADER)

    for key in dico_des_constats:
        ligne = ['-'] * len(HEADER)
        constat = dico_des_constats[key]
        fla_equi = False
        for champ in constat:

            if (type(constat[champ]) is str and
                    constat[champ] and
                    constat[champ][0] == "{"):  # Champ de type local et equi
                valeurs = json.loads(constat[champ])
                if type(valeurs) is dict:
                    for souskey in valeurs:
                        cle = champ + "." + souskey
                        if cle in TRS_CHAMP:
                            ligne[TRS_CHAMP[cle]] = valeurs[souskey]
                            fla_equi = True

            if (champ in TRS_CHAMP and
                    type(constat[champ]) is not dict and
                    ligne[TRS_CHAMP[champ]] == "-"):  # Cas champ brut

                ligne[TRS_CHAMP[champ]] = constat[champ]

            if type(constat[champ]) is dict:
                if ('modele' in constat[champ] and
                        constat[champ]['modele'] in MODELE_EXPORTE):
                    face = constat[champ]
                    nom_face = face["modele"]

                    for champface in face:
                        cle = nom_face + " " + champface
                        if cle in TRS_CHAMP:
                            if champface != "etats" and champface != "etat":
                                val = csvkb.valeur_dict_to_str(face[champface])
                                ligne[TRS_CHAMP[cle]] = val
                            elif champface == "etat":
                                etat = face[champface]['libelle']

                                ligne[TRS_CHAMP[cle]] = etat

        llignes.append(ligne)

    return llignes
