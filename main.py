import os
import sys
import csv
import traceback
import logging
from logging.handlers import RotatingFileHandler

import parsing_csv_kibana
import produire_export_pdf as pdf
import produire_export_full_champ as epo_full

if __name__ == "__main__":

    # Création des fichiers de logs
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formater = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    file_handler = RotatingFileHandler('activity.log', 'a', 1000000, 1)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formater)
    logger.addHandler(file_handler)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    logger.addHandler(stream_handler)

    logger.info('Programme de transformation du fichier csv Kibana vers PDF')

    try:

        fichierin = sys.argv[1]
        nfout = sys.argv[2]

        if len(sys.argv) > 3:
            etat = sys.argv[3]
            print(etat)
        else:
            etat = "Tous"

        dicocst = parsing_csv_kibana.from_kibana_csv_to_pydict(fichierin)

        if etat != 'CSV_BRUT':
            lstcst = pdf.produire_liste_des_constats(dicocst, etat)
        else:
            lstcst = epo_full.export_full(dicocst)

        nfi = open(nfout, 'w', newline='', encoding="cp1252", errors='replace')
        csv_writer = csv.writer(nfi, delimiter=";", dialect="excel")

        for ligne in lstcst:
            try:
                csv_writer.writerow(ligne)
            except:
                print("Référence = "+ligne[0])
                print("Ligne out")

    except:
        logger.info("bug")
        exc_type, exc_value, exc_traceback = sys.exc_info()
        for item in traceback.format_exception(
                        exc_type,
                        exc_value,
                        exc_traceback):
            logger.warning(item)

    logger.info("fin")
