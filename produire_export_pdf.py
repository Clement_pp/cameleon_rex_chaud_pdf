import csv
import parsing_csv_kibana
import pdb

HEADER_EXPORT = ["Référence",
                 "Unité",
                 "Date",
                 "Evénement",
                 "Caractérisation",
                 "SE",
                 "Origine",
                 "Synthèse",
                 "Pesage",
                 "Décisions",
                 "Entité Pilote",
                 "Echéance",
                 "Cadre de Traitement",
                 "Indicateur 2-0",
                 "Cause Prépondérante",
                 "Codification"]

CHAMP_EXPORT_BRUT_COEUR = {
    0: 'reference',
    1: 'localisation.label',
    2: "dateConstat",
    5: 'systemeElementaire',
    6: 'occasionDecouverte'
}

CHAMP_EXPORT_FACE_REX_CHAUD = {
    3: "Libellé de l'évenement",
    4: "caracterisations",
    7: "Synthèse de l'évenement",
    8: "Pesage",
    9: "Décisions",
    10: "Entité pilote",
    11: "Échéance de traitement",
    12: "Cadre de traitement",
    13: "Indicateur 2.0",
    14: "Cause prépondérante"
}

CHAMP_CODIF = [
    "Code evénement",
    "Causes humaines",
    "Causes matérielles",
    "Code équipement",
    "Causes organisationnelles",
    "Code acteur",
    "Code détection"
]


def produire_ligne_export(constat):

    ligne_constat = len(HEADER_EXPORT) * ["-"]

    # Champ du coeur
    for key in CHAMP_EXPORT_BRUT_COEUR:
        if CHAMP_EXPORT_BRUT_COEUR[key] in constat:
            ligne_constat[key] = constat[CHAMP_EXPORT_BRUT_COEUR[key]]

    # Champ de la face REX Chaud
    if 'REX Chaud' in constat:
        for key in CHAMP_EXPORT_FACE_REX_CHAUD:
            nom_du_champ = CHAMP_EXPORT_FACE_REX_CHAUD[key]
            if nom_du_champ in constat['REX Chaud']:

                valeur = constat['REX Chaud'][nom_du_champ]
                valeur = parsing_csv_kibana.valeur_dict_to_str(valeur)
                ligne_constat[key] = valeur

    return ligne_constat


def champ_codification(constat):

    codif = ""

    for champ in CHAMP_CODIF:
        if champ in constat['REX Chaud']:
            code = parsing_csv_kibana.valeur_dict_to_str(
                constat['REX Chaud'][champ])
            codif += code + "\n"

    return codif.strip()


def est_a_ajouter(constat, etat):

    ajout_ou_non = False

    if 'etat' in constat['REX Chaud']:
        etat_cst = parsing_csv_kibana.valeur_dict_to_str(
            constat['REX Chaud']['etat']['code'])

        ajout_ou_non = etat_cst == etat

    return ajout_ou_non


def produire_liste_des_constats(constats, etat="Tous"):

    liste_des_constats = []
    liste_des_constats.append(HEADER_EXPORT)

    for iden in constats:

        if etat != "Tous":
            ajout = est_a_ajouter(constats[iden], etat)
        else:
            ajout = True

        if ajout:
            ligne = produire_ligne_export(constats[iden])

            # Retravail de la date - supprimer l'heure
            if ligne[11] != "-" and len(ligne[11]) > 10:
                ligne[11] = ligne[11][0:10]

            # Concaténation des codification
            ligne[15] = champ_codification(constats[iden])
            liste_des_constats.append(ligne)

            # Filtrage des espaces insécables
            if "&nbsp" in ligne[7]:
                ligne[7] = str(ligne[7]).replace("&nbsp", ' ')

    return liste_des_constats


if __name__ == "__main__":
    pass
